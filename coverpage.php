<?php /*Template name: page with cover*/ ?>
<?php get_header(); ?>
<div id="page-holder">
  <header>
    <i class="material-icons menu" data-toggle="aside">menu</i>
    <div class="mob-head">
      <h1><?php bloginfo('name'); ?></h1>
      <p>Fullstack Designer</p>
    </div>
  </header>
  <?php $post = get_post($_POST['id']); ?>
  <div class="page" id="single-post post-<?php the_ID(); ?>">
    <?php while (have_posts()) : the_post(); ?>
      <figure>
        <span class="p-name">
          <h1><?php the_title(); ?></h1>
        </span>
        <?php the_post_thumbnail(); ?>
      </figure>
    <article>
      <p class="cat-tag">echo get_category();</p>
      <section>
          <p>
          <?php the_content(); ?>
        </p>
      </section>
    </article>
    <?php endwhile; ?>
  </div>
</div>
<div class="swipe-area"></div>
<div class="blur-bg"></div>
<?php get_footer(); ?>
