

<?php $post = get_post($_POST['id']); ?>

<div id="single-post post-<?php the_ID(); ?>">
  <?php while (have_posts()) : the_post(); ?>
  <?php
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
  $thumb_url = $thumb_url_array[0];
  ?>
  <div class="cover">
    <span class="p-name">
      <h1><?php the_title(); ?></h1>
    </span>
    <div class="bg" style="background: url('<?php echo $thumb_url; ?>');"></div>
  </div>
  <article class="p-content">
    <p class="cat-tag"><i class="<?php echo do_shortcode('[cat_ico]'); ?>"></i><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ', '; } ?></p>
    <section>
        <p><?php the_content(); ?></p>
    </section>
  </article>
  <?php endwhile; ?>
</div>
