<?php
	//standarts		global $wp_rewrite;
		$wp_rewrite->flush_rules();		define( 'TEMPPATH',  get_bloginfo('stylesheet_directory'));		define( 'IMAGES', TEMPPATH. "/images/");		define( 'SCRIPTS', TEMPPATH. "/scripts/");		function new_excerpt_length($length) {		    return 5;
		}

		add_filter('excerpt_length', 'new_excerpt_length');
		add_filter('widget_text', 'do_shortcode');		//cover bg		function cat_icon(){			if(in_category('3d')){				echo 'flaticon-geometry21';			}			elseif(in_category('codigo')){					echo 'flaticon-programming2';			}			elseif(in_category('motion')){				echo 'flaticon-heart175';			}			else{				echo 'flaticon-graphicseditor44';			}		}		add_shortcode('cat_ico', 'cat_icon');		//menus
		add_action( 'init', 'register_my_menus' );		function register_my_menus() {
			register_nav_menus(
				array(
					'main-nav' => __( 'Primary Menu' ),
					'cat-menu' => __( 'Food Menu' )
				)
			);
		}

// menu principal
		function menu(){
			wp_nav_menu( array(					'theme_location' => 'main-nav',
					'menu' => '',
					'container' => 'div',
					'container_class' => 'menu-holder',
					'container_id' => '',
					'menu_class' => 'nav-menu',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
			));

		}
		add_shortcode('nav-menu', 'menu');
	// //register scripts		function my_scripts_method( $in_footer ) {
					wp_enqueue_style('flaticons', get_stylesheet_directory_uri() . '/fonts/flaticon.css');
					wp_enqueue_style('awesomeFont', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
					wp_enqueue_style('materialFont', 'https://fonts.googleapis.com/icon?family=Material+Icons');
					//scripts
					// wp_enqueue_script('library', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', array('jquery'));
					wp_enqueue_script('local', get_stylesheet_directory_uri() . '/scripts/min/jquery-1.11.1.min.js', array('jquery'));
					wp_enqueue_script('masonry', get_stylesheet_directory_uri() . '/scripts/min/masonry.pkgd.min.js', array('jquery'));
					wp_enqueue_script('touch', get_stylesheet_directory_uri() . '/scripts/min/jquery.touchSwipe.min.js', array('jquery'));
					wp_enqueue_script('lazy', get_stylesheet_directory_uri() . '/scripts/min/jquery.lazyload.min.js', array('jquery'));
					wp_enqueue_script('effects', get_stylesheet_directory_uri() . '/scripts/min/effects.min.js', array('jquery'));

		}
		add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
//register portfolio
		function custom_post_type() {
			$labels = array(
				'name'                => __( 'Trabajos', 'Trabajos', 'text_domain' ),
				'singular_name'       => __( 'Trabajos', 'Trabajos', 'text_domain' ),
				'menu_name'           => __( 'Trabajos', 'text_domain' ),
				'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
				'all_items'           => __( 'Todas los Trabajos', 'text_domain' ),
				'view_item'           => __( 'Ver Trabajo', 'text_domain' ),
				'add_new_item'        => __( 'Agregar Trabajo', 'text_domain' ),
				'add_new'             => __( 'Nueva Trabajo', 'text_domain' ),
				'edit_item'           => __( 'Edit', 'text_domain' ),
				'update_item'         => __( 'Actualizar Trabajo', 'text_domain' ),
				'search_items'        => __( 'Buscar Trabajo', 'text_domain' ),
				'not_found'           => __( 'no encontrado', 'text_domain' ),
				'not_found_in_trash'  => __( 'no se encontro en la papelera', 'text_domain' ),

			);

			$args = array(

				'label'               => __( 'Trabajos', 'text_domain' ),
				'description'         => __( 'Trabajos', 'text_domain' ),
				'labels'              => $labels,
				'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats' ),
				'taxonomies'          => array( 'category' ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 4,
				'menu_icon'           => 'dashicons-portfolio',
				'can_export'          => true,
				'has_archive'         => 'post',
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'post',



			);

			register_post_type( 'Trabajos', $args );



		}
	// Hook into the 'init' action
		add_action( 'init', 'custom_post_type', 0 );

		//lazyload
	 function footer_lazyload() {
		     echo '
		 <script type="text/javascript">
		     (function($){
		       $("img.lazy").lazyload();
		     })(jQuery);
		 </script>
		 ';
	 }
	 add_action('wp_footer', 'footer_lazyload');

	 function filter_lazyload($content) {
		    return preg_replace_callback('/(<\s*img[^>]+)(src\s*=\s*"[^"]+")([^>]+>)/i', 'preg_lazyload', $content);
		}
		add_filter('the_content', 'filter_lazyload');

	 function preg_lazyload($img_match) {

	    $img_replace = $img_match[1] . 'src="' . get_stylesheet_directory_uri() . '/grey.gif" data-original' . substr($img_match[2], 3) . $img_match[3];

	    $img_replace = preg_replace('/class\s*=\s*"/i', 'class="lazy ', $img_replace);

	    $img_replace .= '<noscript>' . $img_match[0] . '</noscript>';
	    return $img_replace;
	}


//pagination

add_action( 'wp_ajax_nopriv_load-filter', 'prefix_load_cat_posts' );
add_action( 'wp_ajax_load-filter', 'prefix_load_cat_posts' );
function more_post_ajax(){
	//looping
	rewind_posts();
	$paged = get_query_var('page');
	$args = array('post_type' => array('post', 'trabajos'), 'paged' => $paged, 'posts_per_page' => '6');
	$doitbitch = '';


	print '<h1>hola humano</h1>';

	}
	add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
	add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

?>
