<?php get_header(); ?>
<div id="content">
  <header>
    <i class="material-icons menu" data-toggle="aside">menu</i>
    <div class="mob-head">
      <h1><?php echo $blog_title = get_bloginfo('name'); ?></h1>
      <p>Fullstack Designer</p>
    </div>
  </header>
  <div class="content-wrap">

    <?php while ( have_posts() ) : the_post(); ?>
      <article class="item">
        <div class="img-holder">
        <i class="material-icons share">share</i>
          <figure>
            <a rel="<?php the_ID(); ?>" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
          </figure>
          <a class="titulo"rel="<?php the_ID(); ?>" href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2><p> <i class=" cat-ico <?php echo do_shortcode('[cat_ico]') ?>"></i><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ', '; } ?></p></a>
        </div>
        <section>
          <p><?php the_excerpt(); ?></p>
        </section>
        <a href="<?php the_permalink(); ?>"><div class="boton">
          <button>Ver más</button>
        </a>
        </div>
        <div class="sharing">
          <?php echo do_shortcode('[social_share]');?>
        </div>
      </article>
    <?php endwhile ?>
    <div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Previos', 10); ?></div>
			<div class="alignright"><?php previous_posts_link('Recientes &raquo;'); ?></div>
		</div>
    <!-- post-holder -->
    <div id="p-holder">
      <i class="material-icons close">close</i>
      <div class="holder">

      </div>
    </div>

</div>
<div class="swipe-area"></div>
<div class="blur-bg"></div>
