<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es-ES" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"> <![endif]-->

<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="es-ES" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"> <![endif]-->

<!--[if IE 8]>         <html class="no-js lt-ie9" lang="es-ES" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"> <![endif]-->

<!--[if gt IE 8]><!--> <html class="no-js" lang="es-ES" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"> <!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!--    _   _____   _____   _____           ___   _     _   _  _             ___
       | |/  _  \ /  ___/ | ____|         /   | | |   / / | | | |          /   |
      | | | | | | | |___  | |__          / /| | | |  / /  | | | |        / /| |
  _  | | | | | | \___  \ |  __|         / / | | | | / /   | | | |       / / | |
 | |_| | | |_| |  ___| | | |___       / /  | | | |/ /    | | | |___   / /  | |
 \_____/ \_____/ /_____/ |_____|     /_/   |_| |___/     |_| |_____| /_/   |_|

  _   _   _____   _____   _   _   _____        _____   __   _        _     _   _____   __   _   _____   ______  _   _   _____   _           ___
| | | | | ____| /  ___| | | | | /  _  \      | ____| |  \ | |      | |   / / | ____| |  \ | | | ____| |___  / | | | | | ____| | |         /   |
| |_| | | |__   | |     | |_| | | | | |      | |__   |   \| |      | |  / /  | |__   |   \| | | |__      / /  | | | | | |__   | |        / /| |
|  _  | |  __|  | |     |  _  | | | | |      |  __|  | |\   |      | | / /   |  __|  | |\   | |  __|    / /   | | | | |  __|  | |       / / | |
| | | | | |___  | |___  | | | | | |_| |      | |___  | | \  |      | |/ /    | |___  | | \  | | |___   / /__  | |_| | | |___  | |___   / /  | |
|_| |_| |_____| \_____| |_| |_| \_____/      |_____| |_|  \_|      |___/     |_____| |_|  \_| |_____| /_____| \_____/ |_____| |_____| /_/   |_|

                      (\_\_^__o
               ___     `-'/ `_/
              '`--\______/  |
         '        /         |
     `    .  ' `-`/.------'\^-'  -->




	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
	<!-- Chrome, Firefox OS, Opera and Vivaldi -->
	<meta name="theme-color" content="#141414">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#141414">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#141414">
	<title><?php wp_title( '&laquo;', true, 'right' ); ?><?php bloginfo('name'); ?></title>
	<link rel="shortcut icon" href="<?php print IMAGES; ?>favicon.png" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
  <!-- WP header -->
  <div class="wphead">
		<?php wp_head(); ?>
	</div>
</head>
<body>
	<div id="wrap">
		<aside>
			<div class="n-wrap">
				<div class="profile">
					<div class="cover"><img src="<?php the_field('cover', 'option'); ?>" alt="" /></div>
					<figure><a href="<?php echo get_option('home')?> "><img src="<?php the_field('avatar', 'option'); ?>" alt="" /></a></figure>
					<section>
						<h1><a href="<?php echo get_option('home')?>"><?php the_field('nombre', 'option'); ?></a></h1>
						<p class="foco"><?php the_field('campo', 'option'); ?></p>
						<p class="ubicacion"><?php the_field('ubicacion', 'option'); ?></p>
					</section>
				</div>
			<nav>
        <?php echo do_shortcode('[nav-menu]'); ?>
			</nav>
			<div class="contacto">
				<ul>
					<li><a href="mailto:hola@joseavila.com.ve" target="_blank"><i class="fa fa-envelope"></i></a></li>
					<li><a href="https://www.behance.net/zen_" target="_blank"><i class="fa fa-behance"></i></a></li>
					<li><a href="https://ve.linkedin.com/in/avilajose" target="_blank"><i class="fa fa-linkedin" target="_blank"></i></a></li>
					<li><a href="https://www.twitter.com/_zen_design_" target="_blank"><i class="fa fa-twitter"></i></a></li>
				</ul>
				<p class="copyright">&reg; Jose Avila <a href="http://jose-avila.byethost33.com/wp-content/uploads/2016/02/CV2015.pdf">ver CV</a></p>
				<p class="copyright">Potenciado por <a href="http://wordpress.org"><i class="fa fa-wordpress"></i></a> </p>
			</div>
		</div>
		</aside>
