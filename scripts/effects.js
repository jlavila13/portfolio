$(document).ready(function(){
	$('#content').masonry({
		// options
		itemSelector: '.item',
		// use element for option
	 percentPosition: true
		// columnWidth: 300
	});

	$('.item  a').click(function(){
		$('#p-holder').show().animate({
			'right': '0px'
		},175);
		$('.blur-bg').addClass('bg-on');
		$('body').addClass('lock-body');
		return false

	});
	$('.close').click(function(){
		// alert('click coño click!!!');
		$('#p-holder').show().animate({
			'right': '-8000px'
		},50);
		$('.blur-bg').removeClass('bg-on');
		$('body').removeClass('lock-body');
	});
	$('.share').click(function(){
		// alert('click coño click!!!');
		$(this).find('.sharing').fadeToggle();
	});
	$('.blur-bg').click(function(){
		$('#p-holder').show().animate({
			'right': '-8000px'
		},50);
		$('.blur-bg').removeClass('bg-on');
		$('body').removeClass('lock-body');
	});
});

//toogle menu
 $(window).load(function(){
	 $("[data-toggle]").click(function() {
		 var toggle_el = $(this).data("toggle");
		 $(toggle_el).toggleClass("open-sidebar");
		 $('body').toggleClass('lock-body');
		 $('.swipe-area').toggleClass('swipe-on');
		 $('.menu').toggleClass('on');
		 $('.blur-bg').toggleClass('bg-on');
	 });
	 $(".swipe-area").swipe({
			 swipeStatus:function(event, phase, direction, distance, duration, fingers)
					 {
							 if (phase=="move" && direction =="right") {
										$('aside').addClass('open-sidebar');
										$('.menu').addClass('on');
										$("body").addClass("lock-body");
										$(".swipe-area").addClass("swipe-on");
										$(".blur-bg").addClass("bg-on");
										return false;
							 }
							 else if (phase=="move" && direction =="left") {
										 $('aside').removeClass('open-sidebar');
										 $('.menu').removeClass('on');
										 $("body").removeClass("lock-body");
										 $(".swipe-area").removeClass("swipe-on");
										 $(".blur-bg").removeClass("bg-on");
										return false;
							 }
					 }
 });
 });

	//ajax posts loader
	$(document).ready(function(){
		 $.ajaxSetup({cache:false});
		 $(".item a").click(function(){
				 var post_link = $(this).attr("href");
				 $(".holder").html("<i class='fa fa-spinner fa-pulse'></i>");
				 $(".holder").load(post_link);
		 return false;
		 });

		 $.ajaxSetup({cache:false});
		 $("nav a").click(function(){
			 var post_link = $(this).attr("href");
			 $(".content-wrap").html('<i class="fa fa-spinner fa-pulse"></i>');
			 $(".content-wrap").load(post_link);
			 return false;
		 });
	 });

//some function of massonry
	$(function() {
	  var masonryInit = true;	// set Masonry init flag
	  $.fn.almComplete = function(alm){ // Ajax Load More callback function
	    if($('.content-wrap').length){
	      var $container = $('.content-wrap article'); // our container
	      if(masonryInit){
	        // initialize Masonry only once
	        masonryInit = false;
	        $container.masonry({
	          itemSelector: '.item'
	        });
	      }else{
	          $container.masonry('reloadItems'); // Reload masonry items oafter callback
	      }
	      $container.imagesLoaded( function() { // When images are loaded, fire masonry again.
	        $container.masonry();
	      });
	    }
	  };
	});

//ajax load more
$(document).ready(function(){
	//ajax
	var ppp = 3; // Post per page
	var cat = 8;
	var pageNumber = 1;
	function load_posts(){
		pageNumber++;
		var str = '&cat=' + cat + '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_post_ajax';
		$.ajax({
			type: "POST",
			url     : '/testsite/wp-admin/admin-ajax.php',
			data: str,
			success: function(data){
				var $data = $(data);
				if($data.length){
					$("#ajax-posts").append($data);
					$("#more_posts").attr("disabled",false);
				} else{
					$("#more_posts").attr("disabled",true);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
			}

		});
		return false;
	}

	$("#more_posts").on("click",function(){ // When btn is pressed.
		$("#more_posts").attr("disabled",true); // Disable the button, temp.
		load_posts();
	});
});
