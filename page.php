<?php get_header(); ?>
<div id="page-holder">
  <?php  $post = get_post($_POST['id']); ?>
  <div class="content-wrap page" id="single-post post-<?php the_ID(); ?>">
    <header>
      <i class="material-icons menu" data-toggle="aside">menu</i>
      <div class="mob-head">
        <h1><?php bloginfo('name'); ?></h1>
        <p>Fullstack Designer</p>
      </div>
    </header>
    <?php while (have_posts()) : the_post(); ?>
      <h1><?php the_title(); ?></h1>
    <article>
      <section>
          <p>
          <?php the_content(); ?>
        </p>
      </section>
    </article>
    <?php endwhile; ?>
  </div>
</div>
<div class="swipe-area"></div>
<div class="blur-bg"></div>
<?php get_footer(); ?>
